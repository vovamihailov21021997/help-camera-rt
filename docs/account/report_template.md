# Шаблон отчета

Шаблон — XLSX-файл, содержащий нужные элементы оформления для вашего отчета, например, диаграммы, созданные на основе данных отчета, выделения, ваши собственные названия для параметров и листов.
В качестве шаблона рекомендуется использовать скачанный и отредактированный файл отчета, для которого вы создаете шаблон.

## Создание шаблона

1. Скачайте отчет.
1. Откройте полученный файл.
1. Отредактируйте его, добавив и изменив нужные элементы.
1. Сохраните файл.

## Загрузка отчета в систему

1. Откройте страницу отчета в боковом меню личного кабинета.
1. Нажмите «Загрузить шаблон».
1. Выберите файл шаблона в открывшемся окне.
1. Сохраните его.

## Скачивание отчета, оформленного по шаблону
Процесс скачивания отчета, оформленного по шаблону, ничем не отличается от скачивания обычного отчета. Укажите даты, при необходимости отредактируйте параметры и нажмите «Скачать отчет». Загруженный файл будет оформлен по шаблону.

## Удаление и замена шаблона
Чтобы удалить шаблон:

1. Откройте страницу отчета в боковом меню личного кабинета.
1. Нажмите «Удалить шаблон».

После этого отчет будет скачиваться неоформленным.
Чтобы заменить шаблон, удалите старый и загрузите новый.
