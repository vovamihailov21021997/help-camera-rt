# Модели камер и локальных регистраторов, которые поддерживает система

## Hikvision/HiWatch

* DS-2CD2432
* DS-2CD2042WD-I (4mm)
* DS-2CD2742FWD-IZS
* DS-2CD2642FWD-IZS
* DS-2CD2043G0-I (4mm)
* DS-2CD2422FWD-IW (2.8mm)
* DS-2CD2F22FWD-IS (2.8mm)
* DS-2CD2022WD-I (4mm)
* DS-2CD2122FWD-IS (2.8mm)
* DS-2CD2622FWD-IZS
* DS-2CD2522FWD-IWS (2.8mm)
* DS-I114 (2.8 mm)
* DS-I114W (2.8 mm)
* DS-I120 (4 mm)
* DS-I122 (2.8 mm)
* DS-I126 (2.8-12 mm)
* DS-I128 (2.8-12 mm)
* DS-2CD2622FWD-IS
* DS-2CD2722FWD-IS
* DS-2CD2722FWD-IZS
* DS-2CD2142FWD-IS (2.8mm)
* DS-2CD2442FWD-IW (2.8mm)
* DS-2CD2542FWD-IS (2.8mm)
* DS-2CD2542FWD-IWS (2.8mm)
* DS-2CD2642FWD-IS
* DS-2CD2742FWD-IS
* DS-2DE3204W-DE
* DS-2DE4225W-DE
* DS-2DE4225W-DE3
* DS-2CD2425FWD-IW (2.8mm)
* DS-2CD2525FHWD-IS (2.8mm)
* DS-2CD2525FHWD-IWS (2.8mm)
* DS-2CD2023G0-I (2.8mm)
* DS-2CD2123G0-IS (2.8mm)
* DS-2CD2423G0-IW (2.8mm)
* DS-2CD2623G0-IZS
* DS-2CD2523G0-IS (2.8mm)
* DS-2CD2625FHWD-IZS (2.8-12mm)
* DS-2CD2725FHWD-IZS (2.8-12mm)
* DS-2CD2655FWD-IZS (2.8-12mm)
* DS-2CD2755FWD-IZS (2.8-12mm)
* DS-2CD2035FWD-I (4mm)
* DS-2CD2135FWD-IS (2.8mm)

## Dahua

* DH-IPC-HDBW2431RP-ZAS
* DH-IPC-HFW2431TP-ZAS
* DH-IPC-HFW4431EP-SE-0360B
* DH-IPC-K26
* DH-IPC-A26
* DH-IPC-HFW1230SP-0360B
* DH-IPC-HDW1230SP-0360B
* DH-IPC-HFW5231EP-ZE
* DH-IPC-HDBW4231FP-AS-0280B

## RTSP-камеры

Возможно добавить сторонние IP-камеры, поддерживающие RTSP-протокол. Для этого
необходимо, чтобы:

1.  Битрейт был постоянным (CBR);
2.  Битрейт не превышал 1.5 МБит/c;
3.  Видеокодек — H.264;
4.  Частота кадров не более 30 к/c;
5.  IP-адрес в формате IPv4;
6.  Аудиокодеки — AAC или G.711.

## Сетевые видеорегистраторы (NVR) Hikvision

* DS-7604NI-K1
* DS-7604NI-K1/4P
* DS-7608NI-K2
* DS-7608NI-K2/8P
* DS-7616NI-K2
* DS-7616NI-K2/16P
