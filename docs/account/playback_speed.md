# Ускорение и замедление видео
Архивное видео при просмотре можно ускорить и замедлить.

1. Перейдите к просмотру архива — нажите на таймлайн или выберите дату в прошлом в календаре под таймлайном.
1. Нажмите на кнопку «Изменить скорость», которая появилась слева от кнопки управления звуком. Когда вы смотрите архив с той скоростью, с которой он записан, на кнопке «Изменить скорость» написано «x1».
1. Выберите коэффициент ускорения/замедления видео и нажмите на него:
* «0.25» — замедление в 4 раза;
* «0.5» — замедление в 2 раза;
* «1» — нормальная скорость видео;
* «1.5» — ускорение в полтора раза;
* «2» — ускорение в 2 раза;
* «4» — ускорение в 4 раза, доступно во всех браузерах, кроме Internet Explorer;
* «Макс.» — максимально возможное для потока ускорение, обычно от 25 до 125 раз.

Чтобы вернуться к нормальной скорости потока, выберите коэффициент «1».
