# Удаление раскладки

1. Откройте список раскладок.
1. Нажмите на крестик в правом верхнем углу блока раскладки в списке.
1. Подтвердите действие.

Удаленную раскладку нельзя восстановить. Удаление раскладки никак не влияет на камеры, включенные в нее.
