# Экран камеры

На экране камеры находится ее плеер, в котором показывается видео с камеры.

![Экран с плеером камеры](./images/3.5.0.0-b2b2с-screen_with_camera_player.jpg "Экран с плеером камеры")

В режимах приложения «Облако» и «Сеть» доступен переход на экран любой камеры, а в режиме «Только сеть» можно открыть только локальные камеры (подробнее в разделе [«Ограничения функций в режиме офлайн»](../application_modes/index.md).

## Управление просмотром камеры

Под плеером расположен таймлайн для управления просмотром.

![Таймлайн камеры](./images/3.5.0.0-b2b2с-timeline_cameras.jpg "Таймлайн камеры")

Пример использования кнопок изменения масштаба (2), а также перемещения по таймлайну (3) и (5): на картинке выше на таймлайне отображается интервал времени 24 часа (примерно с 18:40 до 18:40 следующего дня).

Тогда:

* нажатие кнопки (3) сдвинет таймлайн на длину таймлайна (24 часа) назад,
* нажатие кнопки (4) сдвинет таймлайн на длину таймлайна (24 часа)  вперёд,
* нажатие кнопки (2) изменения масштаба (+) изменит длину таймлайна до 12 часов (с 18:40 до 6:40 следующего дня),
* нажатие кнопки (2) изменения масштаба (-) изменит длину таймлайна до 48 часов (с 18:40 до 18:40 дня после следующего).

Полный список подсвеченных на скриншоте кнопок:

* (1) - таймлайн, на котором цветовыми отметками указаны произошедшие события; цвет отметки соответствует цвету события, а длина отметки - длительности.
* (2) - кнопки для изменения масштаба таймлайна,
* (3) - перейти на 1 длину таймлайна назад (см. комментарий ниже),
* (4) - кнопка паузы-воспроизведения,
* (5) - перейти на 1 длину таймлайна вперёд (см. комментарий ниже),
* (6) - кнопка перехода в Live-трансляции,
* (7) - элемент для выбора даты и времени для воспроизведения видео; в режиме Live трансляции показывает значение "СЕЙЧАС",
* (8) - изменение скорости воспроизведения; доступно только при воспроизведении архивного видео,
* (9) - кнопка включения режима получения и воспроизведения только ключевых кадров (для экономии траффика и/или ресурсов ПК).
* (10) - переключатель воспроизводимого потока (HQ - основной, LQ - дополнительный).
* (11) - кнопки перемещения по таймлайну.

Чтобы перейти к просмотру архивного видео от камеры, нажмите на нужное время на таймлайне или укажите дату и время, видео за которые нужно просмотреть, нажав на кнопку «Сегодня» и вызвав календарь.

Как скрыть таймлайн в полноэкранном режиме описано в разделе [«Настройки»](../settings/index.md).

Просмотр камеры и управление им доступны для тех камер, видео от которых поступает в приложение в настоящий момент. Подробнее о влиянии режимов "Приоритет облака" и "Приоритет локальной сети" на то, из какого источника получается видео с камеры, описано в разделе [«Выбор приоритетного источника видео»](../video_source/index.md).

## Режимы «Дополнительный поток» и «Ключевые кадры»

Чтобы уменьшить нагрузку на процессор при воспроизведении большого числа видео, вы можете воспользоваться режимами «Дополнительный поток камеры» и «Ключевые кадры». Последний режим особенно актуален, если у вас медленное соединение с интернетом.

Обратите внимание, что указанные режимы работают независимо друг от друга. Также доступность этих режимов зависит от возможностей камеры и её способа подлючения.

Например, для камер, подключённых через RTSP-ссылку нет возможности переключиться на дополнительный поток - а режим «Ключевые кадры» будет доступен тольков режиме «Облако».

**Режим «Дополнительный поток»** уменьшает качество видео, поступающего с камеры. Посмотреть и изменить характеристики дополнительного потока для камеры можно в разделе [«Настройки устройств»](../settings/index.md).

Чтобы изменить качество потока, используйте кнопку «HQ / LQ» в правой части панели управления воспроизведением. HQ (high quality) — основной поток, LQ (low quality) — дополнительный поток.

![Переключение между режимами «Основной поток» и «Дополнительный поток»](./images/3.7.0.56-b2b-additional_stream.png)

**В режиме «Ключевые кадры»** в плеере будут показаны только ключевые кадры видеопотока камеры (по умолчанию только каждый 125й кадр).

Пример: если на камере установлена частота кадров 25fps, в режиме «Ключевые кадры» вы будете видеть 1 кадр каждые `125 / 25` = 5 секунд.

Чтобы включать и выключать режим «Ключевые кадры», используйте кнопку в виде улитки в правой части панели управления воспроизведением. 

![Включение режима «Ключевые кадры»](./images/3.7.0.56-b2b-key_frames.png)

По умолчанию режим «Ключевые кадры» можно включить только при воспроизведении видео из облака. При воспроизведении по локальной сети этот режим будет работать только на камерах с версией прошивки v2.15.0 и более поздней. Если вам требуется включить режим «Ключевые кадры» для видео, воспроизводящегося по локальной сети, включите функцию «Подключаться к источнику по протоколу spif-proto при воспроизведении через облако» в [общих настройках Настольного приложения](../settings/index.md).

![Включение режима «Ключевые кадры» для видео, воспроизводящегося через облако](./images/3.7.0.56-b2b-spif-proto_cloud.png)

## Просмотр информации о камере и переход в полноэкранный режим

![Окно с информацией о камере и переход в полноэкранный режим](./images/3.5.0.0-b2b2с-camera_information_window.jpg "Окно с информацией о камере и переход в полноэкранный режим")

Чтобы перейти в широкоэкранный режим или выйти из него, дважды кликните на видео с камеры или нажмите кнопку «На весь экран». 

Чтобы вызвать окно с информацией о камере, нажмите кнопку «i». Появится всплывающее окно, в котором указаны характеристики камеры и установленного на ней программного обеспечения.

Из окна с информацией о камере можно перейти на эту же камеру в ЛК на сайте [camera.rt.ru](https://camera.rt.ru).

## Переход к странице камеры на сайте

В приложении нельзя изменять настройки камеры, но вы можете перейти на сайт camera.rt.ru и поменять настройки камеры там. Для этого на ПК должен быть установлен браузер.

1. Нажмите кнопку с буквой «i» в плеере камеры.
2. Нажмите «Открыть Web».

Приложение запустит браузер на ПК, чтобы открыть сайт, или воспользуется уже запущенным. При этом вводить логин-пароль не требуется - ПО автоматически авторизует вас.

Переход к странице камеры на сайте доступен только в режиме «Облако» и при наличии подключения к Интернету.
